export class Student{
    fullName: string;
    physicsMarks: number;
    chemistryMarks: number;
    mathsMarks: number;
    englishMarks: number;
    totalMark: number;
    grade: string;
    constructor(name){
        this.fullName = name,
        this.physicsMarks = 0,
        this.chemistryMarks = 0,
        this.mathsMarks = 0,
        this.englishMarks = 0,
        this.totalMark = 0,
        this.grade =""
    }
}
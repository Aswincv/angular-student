import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { DataProcessingService } from './data_processing.service';

@Injectable()
export class DataStorageService{
    constructor(private http: HttpClient, private dataPro: DataProcessingService){}
    fetchDataFromServer(){
        return this.http.get('https://us-central1-ntgapp-e3318.cloudfunctions.net/studentList').subscribe(
            (studentsData: {'firstName': string,'lastName': string, 'subject': string, 'marks': number}[]) => {
            if(this.dataPro.studentsData.length == 0){
                this.dataPro.setRowData(studentsData);
            }
        }
        )
    }
}

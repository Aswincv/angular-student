import { Student } from '../models/app.model';

export class DataProcessingService{
    rowData: {'firstName': string,'lastName': string, 'subject': string, 'marks': number}[] = [];
    studentsData: Student[] = [];
    passMarkObj:{} = {
      'physics': 40,
      'maths': 40,
      'chemistry': 35,
      'english': 35
    }
    
    constructor(){}
    
    getSubjectResult(subjectName: string, mark: number){
      return mark >= this.passMarkObj[subjectName] ? true : false;
    }

    getStudentDataForName(name: string){
      let index: number = -1;
      for(let i=0; i < this.studentsData.length ; i++){
          if (this.studentsData[i].fullName === name) {
              index = i;
              break;
          }
      }
      if(index >= 0){
        return this.studentsData[index];
      }else{
        return new Student('Not Found');
      }
    }

    getStudentData(){
        return this.studentsData;
    }

    setRowData(rowData: {'firstName': string,'lastName': string, 'subject': string, 'marks': number}[]){
        this.rowData = rowData;
        this.processData()
    }

    processData(){
      let tempListNames: string[] = [];
      for(let row of this.rowData){
        let fullName = row.firstName +row.lastName;
        if (!tempListNames.includes(fullName)){
          console.log(fullName, tempListNames)
          let st = new Student(fullName);
          if (row.subject.toLowerCase() === 'physics'){
            st.physicsMarks = row.marks;
          }
          if (row.subject.toLowerCase() === 'chemistry'){
            st.chemistryMarks = row.marks;
          }
          if (row.subject.toLowerCase() === 'maths'){
            st.mathsMarks = row.marks;
          }
          if (row.subject.toLowerCase() === 'english'){
            st.englishMarks = row.marks;
          }
          this.studentsData.push(st)
          tempListNames.push(fullName)
        } else{
          let index: number;
          for(let i=0; i < this.studentsData.length ; i++){
            if (this.studentsData[i].fullName === fullName) {
              index = i;
              break;
            }
          }
          if (row.subject.toLowerCase() === 'physics'){
            this.studentsData[index].physicsMarks = row.marks;
          }
          if (row.subject.toLowerCase() === 'chemistry'){
            this.studentsData[index].englishMarks = row.marks;
          }
          if (row.subject.toLowerCase() === 'maths'){
            this.studentsData[index].mathsMarks = row.marks;
          }
          if (row.subject.toLowerCase() === 'english'){
            this.studentsData[index].englishMarks = row.marks;
          }
        }
      }
      for (let student of this.studentsData){
        student.totalMark = this.getTotalMarks(student)
        student.grade = this.getGrade(student)
      }
    }
    getTotalMarks(student: Student){
        return student.chemistryMarks+student.mathsMarks+student.physicsMarks+student.englishMarks;
      }
      getGrade(student: Student){
        let percentage: number = this.getTotalMarks(student)/4;
        if (percentage>=90){
          return "DISTINCTION";
        }
        if (percentage>=75){
          return "FIRST CLASS";
        }
        if (percentage>=60){
          return "SECOND CLASS";
        }
        else{
          return "BELOW SECOND CLASS"
        }
      }
}
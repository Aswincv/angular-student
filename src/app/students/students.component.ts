import { Component, OnInit } from '@angular/core';
import { DataProcessingService } from '../shared/services/data_processing.service';
import { DataStorageService } from '../shared/services/data_storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  getSubjectResult(subject: string, mark: number){
    return this.dataProcess.getSubjectResult(subject, mark);
  }
  
  getData(){
    return this.dataProcess.getStudentData();
  }

  constructor(public dataStore: DataStorageService,
    public dataProcess: DataProcessingService,
    public router:Router) {
    }
  ngOnInit(): void {
    this.dataStore.fetchDataFromServer()
  }

}

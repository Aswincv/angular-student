import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Student } from '../shared/models/app.model';
import { DataProcessingService } from '../shared/services/data_processing.service'
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  studentData: Student;

  getSubjectResult(subject: string, mark: number){
    return this.processedData.getSubjectResult(subject, mark);
  }
  
  getStudentData(fullName: string){
    this.studentData = this.processedData.getStudentDataForName(fullName);
  }

  constructor(public router:ActivatedRoute,public processedData: DataProcessingService) {
    this.router.params.subscribe( (params:{name: string}) => {
      this.getStudentData(params.name);
    } );
   }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsComponent } from './students/students.component';
import { StudentComponent } from './student/student.component';

const appRoutes: Routes = [
    { path: '', redirectTo: '/students', pathMatch: 'full'},
    { path: 'students', component: StudentsComponent},
    // { path: 'student', component: StudentComponent},
    { path: 'students/:name', component: StudentComponent},
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}
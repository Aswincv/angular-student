import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { StudentsComponent } from './students/students.component';
import { DataStorageService } from './shared/services/data_storage.service';
import { DataProcessingService } from './shared/services/data_processing.service';
import { StudentComponent } from './student/student.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    DataStorageService,
    DataProcessingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
